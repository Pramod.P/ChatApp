package com.inscript.chatapp.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * Created by pramodpatil305 on 15-02-2018.
 */

public class SessionManager {

    public static String USER_ID = "userId";
    public static String ID = "id";

    SharedPreferences preferences;

    public SessionManager(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public String getStringValue(String key) {
        Log.i("SessionManager", key + "-" + preferences.getString(key, " ") + " Retrieve");
        return preferences.getString(key, "");
    }

    public void setStringValue(String key, String value) {
        try {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(key, value);
            editor.commit();
            editor.apply();
            Log.i("SessionManager", key + "-" + value + " Saved");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearData()
    {
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
        editor.apply();
    }
}
