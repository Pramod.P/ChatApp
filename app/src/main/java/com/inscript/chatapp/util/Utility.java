package com.inscript.chatapp.util;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.SwipeRefreshLayout;

import com.inscript.chatapp.R;


/**
 * Created by pramodpatil305 on 13-06-2018.
 */

public class Utility
{
    public static final String CHARACTER_POJO = "character_pojo";
    public static final String EXAMID = "examId";
    public static final String SUBJECTID="subjectId";
    public static final String CHAPTERS = "chapters";
    public static final String MOBILE_NO = "mobile_no";
    public static ProgressDialog progressDialog;

    public static void showProgressDialog(Context context, String message) {
        try {
            if (progressDialog == null)
                progressDialog = new ProgressDialog(context);

            if (!progressDialog.isShowing()) {
                if (message != null)
                    progressDialog.setMessage(message);
                else
                    progressDialog.setMessage("Please wait...");
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void dismissDialog()
    {
        try {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void showTryAgainDialog(Context context, final DialogListeners listener)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Star Wars");
        builder.setMessage("Some problem occurred while retrieving data from the server.");
        builder.setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.onTryAgain(dialog);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.onCancel(dialog);
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

   public interface DialogListeners
    {
        void onTryAgain(DialogInterface dialog);
        void onCancel(DialogInterface dialog);
    }

    public void internetErrorDialog(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Star Wars");
        builder.setMessage("Please connect to working internet connection.");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                dismissDialog();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void dismissSwipeLayout(SwipeRefreshLayout swipeRefreshLayout)
    {
        if(swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
    }

    public static void showNotification(String strOtp, Context context)
    {
        NotificationCompat.Builder n = new NotificationCompat.Builder(context)
                .setContentTitle("DailyDose")
                .setContentText("Your OTP is "+strOtp)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setVibrate(new long[] { 1000, 1000})
                .setLights(Color.RED, 3000, 3000);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, n.build());
    }

}
