package com.inscript.chatapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.inscript.chatapp.R;
import com.inscript.chatapp.model.UserProfile;
import com.inscript.chatapp.activity.chat.ChatActivity;

import java.util.ArrayList;

/**
 * Created by pramodpatil305 on 30-04-2018.
 */

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyViewHolder>
{
    ArrayList<UserProfile> commonModelArrayList;
    MyViewHolder myViewHolder;
    Fragment fragment;
    static Context context;
    RecyclerView recyclerView;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_item, parent, false);

        myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        myViewHolder.tvUserName.setText(commonModelArrayList.get(position).getN());
        myViewHolder.tvStatus.setText(commonModelArrayList.get(position).getM());
        Glide.with(context).load("http:"+commonModelArrayList.get(position).getA())
                .apply(new RequestOptions()
                        //  .bitmapTransform(new CircleTransform(getActivity()))
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(myViewHolder.imgProfile);

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, ChatActivity.class);
            intent.putExtra("userId",commonModelArrayList.get(position).getId());
            intent.putExtra("msgId","0");
            intent.putExtra("name",commonModelArrayList.get(position).getN());
            context.startActivity(intent);
        }
    });
    }

    public UserAdapter(ArrayList<UserProfile> userProfileArrayList,Context context,
                       RecyclerView recyclerView)
    {
        this.fragment = fragment;
        this.commonModelArrayList = userProfileArrayList;
        this.context = context;
        this.recyclerView = recyclerView;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvUserName,tvStatus;
        ImageView imgProfile;

        public MyViewHolder(View view)
        {
            super(view);
            tvUserName = (TextView) view.findViewById(R.id.tvUserName);
            tvStatus = (TextView) view.findViewById(R.id.tvStatus);
            imgProfile = (ImageView) view.findViewById(R.id.imgUser);
        }
    }

    @Override
    public int getItemCount() {
        return commonModelArrayList.size();
    }

}
