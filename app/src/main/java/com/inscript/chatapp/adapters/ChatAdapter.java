package com.inscript.chatapp.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.inscript.chatapp.R;
import com.inscript.chatapp.model.Message;
import com.inscript.chatapp.util.SessionManager;

import java.util.ArrayList;

/**
 * Created by pramodpatil305 on 30-04-2018.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyViewHolder>
{
    ArrayList<Message> commonModelArrayList;
    MyViewHolder myViewHolder;
    Fragment fragment;
    static Context context;
    RecyclerView recyclerView;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_item, parent, false);

        myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        String userId = new SessionManager(context).getStringValue(SessionManager.ID);
        if(userId.equals(commonModelArrayList.get(position).getFrom()))
        {
            myViewHolder.llLeft.setVisibility(View.GONE);
            myViewHolder.llRight.setVisibility(View.VISIBLE);

            if(commonModelArrayList.get(position).getMessage_type().equals("10")) {
                myViewHolder.tvRight.setText(commonModelArrayList.get(position).getMessage());
                myViewHolder.tvRight.setVisibility(View.VISIBLE);
                myViewHolder.imgRightMsg.setVisibility(View.GONE);
            }
            else
            {
                Glide.with(context).load(commonModelArrayList.get(position).getMessage())
                        .apply(new RequestOptions()
                                //  .bitmapTransform(new CircleTransform(getActivity()))
                                .diskCacheStrategy(DiskCacheStrategy.ALL))
                        .into(myViewHolder.imgRightMsg);
                myViewHolder.tvRight.setVisibility(View.GONE);
                myViewHolder.imgRightMsg.setVisibility(View.VISIBLE);
            }
        }
        else
        {
            myViewHolder.llRight.setVisibility(View.GONE);
            myViewHolder.llLeft.setVisibility(View.VISIBLE);

            if(commonModelArrayList.get(position).getMessage_type().equals("10")) {
                myViewHolder.tvLeft.setText(commonModelArrayList.get(position).getMessage());
                myViewHolder.tvLeft.setVisibility(View.VISIBLE);
                myViewHolder.imgLeftMsg.setVisibility(View.GONE);
            }
            else
            {
                Glide.with(context).load(commonModelArrayList.get(position).getMessage())
                        .apply(new RequestOptions()
                                //  .bitmapTransform(new CircleTransform(getActivity()))
                                .diskCacheStrategy(DiskCacheStrategy.ALL))
                        .into(myViewHolder.imgLeftMsg);
                myViewHolder.tvLeft.setVisibility(View.GONE);
                myViewHolder.imgLeftMsg.setVisibility(View.VISIBLE);
            }

        }
    }

    public ChatAdapter(ArrayList<Message> MessageArrayList, Context context,
                       RecyclerView recyclerView)
    {
        this.fragment = fragment;
        this.commonModelArrayList = MessageArrayList;
        this.context = context;
        this.recyclerView = recyclerView;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvRight,tvLeft;
        LinearLayout llRight,llLeft;
        ImageView imgLeftMsg, imgRightMsg;

        public MyViewHolder(View view)
        {
            super(view);
            tvRight = (TextView) view.findViewById(R.id.tvRightMsg);
            tvLeft = (TextView) view.findViewById(R.id.tvLeftMsg);
            llRight = (LinearLayout) view.findViewById(R.id.llRightMsg);
            llLeft = (LinearLayout) view.findViewById(R.id.llLeftMsg);
            imgLeftMsg = (ImageView) view.findViewById(R.id.imgLeftMsg);
            imgRightMsg = (ImageView) view.findViewById(R.id.imgRightMsg);
        }
    }

    @Override
    public int getItemCount() {
        return commonModelArrayList.size();
    }

}
