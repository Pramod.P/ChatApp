package com.inscript.chatapp.model;

import java.io.Serializable;

import fm.icelink.ServerAddress;

public class UserProfile implements Serializable
{
 //   {"id":"46","n":"Iron Man","l":"","a":"\/\/fast.cometondemand.net\/admin\/images\/pixel.png","s":"available","m":"I'm available","ch":"be76292ad741f02dc220db931cd8f472","ls":"1530426991","lstn":"0","rdrs":"1","push_channel":"C_0fa544fceade8056b6d3fca160054018a","ccmobileauth":0,"push_an_channel":"ANN_771cacac13a550b3ab0e7943bb20fc04a","webrtc_prefix":"a4eacdf08e8fda83c7784c8fd21f7811"}
 private String ls;

    private String rdrs;

    private String a;

    private String push_an_channel;

    private String n;

    private String l;

    private String m;

    private String id;

    private String push_channel;

    private String webrtc_prefix;

    private String lstn;

    private String s;

    private String ccmobileauth;

    private String ch;

    public String getLs ()
    {
        return ls;
    }

    public void setLs (String ls)
    {
        this.ls = ls;
    }

    public String getRdrs ()
    {
        return rdrs;
    }

    public void setRdrs (String rdrs)
    {
        this.rdrs = rdrs;
    }

    public String getA ()
    {
        return a;
    }

    public void setA (String a)
    {
        this.a = a;
    }

    public String getPush_an_channel ()
    {
        return push_an_channel;
    }

    public void setPush_an_channel (String push_an_channel)
    {
        this.push_an_channel = push_an_channel;
    }

    public String getN ()
    {
        return n;
    }

    public void setN (String n)
    {
        this.n = n;
    }

    public String getL ()
    {
        return l;
    }

    public void setL (String l)
    {
        this.l = l;
    }

    public String getM ()
    {
        return m;
    }

    public void setM (String m)
    {
        this.m = m;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getPush_channel ()
    {
        return push_channel;
    }

    public void setPush_channel (String push_channel)
    {
        this.push_channel = push_channel;
    }

    public String getWebrtc_prefix ()
    {
        return webrtc_prefix;
    }

    public void setWebrtc_prefix (String webrtc_prefix)
    {
        this.webrtc_prefix = webrtc_prefix;
    }

    public String getLstn ()
    {
        return lstn;
    }

    public void setLstn (String lstn)
    {
        this.lstn = lstn;
    }

    public String getS ()
    {
        return s;
    }

    public void setS (String s)
    {
        this.s = s;
    }

    public String getCcmobileauth ()
    {
        return ccmobileauth;
    }

    public void setCcmobileauth (String ccmobileauth)
    {
        this.ccmobileauth = ccmobileauth;
    }

    public String getCh ()
    {
        return ch;
    }

    public void setCh (String ch)
    {
        this.ch = ch;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ls = "+ls+", rdrs = "+rdrs+", a = "+a+", push_an_channel = "+push_an_channel+", n = "+n+", l = "+l+", m = "+m+", id = "+id+", push_channel = "+push_channel+", webrtc_prefix = "+webrtc_prefix+", lstn = "+lstn+", s = "+s+", ccmobileauth = "+ccmobileauth+", ch = "+ch+"]";
    }
}
