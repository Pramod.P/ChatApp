package com.inscript.chatapp.model;

import java.util.Date;

public class Message implements Comparable<Message>
{
    String id, from,message,self,old,sent,message_type,to,direction;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getOld() {
        return old;
    }

    public void setOld(String old) {
        this.old = old;
    }

    public String getSent() {
        return sent;
    }

    public void setSent(String sent) {
        this.sent = sent;
    }

    public String getMessage_type() {
        return message_type;
    }

    public void setMessage_type(String message_type) {
        this.message_type = message_type;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    @Override
    public int compareTo(Message o) {
        if (new Date(Long.parseLong(getSent())) == null || new Date(Long.parseLong(o.getSent())) == null)
            return 0;
        return new Date(Long.parseLong(getSent())).compareTo(new Date(Long.parseLong(o.getSent())));
    }
}
