package com.inscript.chatapp.activity.login;

import android.content.Context;

import com.inscripts.interfaces.Callbacks;

import org.json.JSONObject;

import cometchat.inscripts.com.cometchatcore.coresdk.CometChat;


/**
 * Created by pramodpatil305 on 21-06-2018.
 */

public class LoginModel implements LoginContracter.LModel
{
    LoginContracter.LPresenter presenter;

    public LoginModel(LoginContracter.LPresenter lPresenter)
    {
        this.presenter = lPresenter;
    }

    @Override
    public void verifyLogin(String uid, Context context) {

        try {
            CometChat cometChat = CometChat.getInstance(context);

            cometChat.loginWithUID(context,uid, new Callbacks() {
                @Override
                public void successCallback(JSONObject jsonObject) {
                    presenter.getResponse(jsonObject);
                }
                @Override
                public void failCallback(JSONObject jsonObject) {
                    presenter.getResponse(jsonObject);
                }
            });

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

  /*  @Override
    public void logout(String uid, Context context) {

        try {
            CometChat cometChat = CometChat.getInstance(context);

            cometChat.logout(new Callbacks() {
                @Override
                public void successCallback(JSONObject jsonObject) {
                    presenter.getResponse(jsonObject);
                }

                @Override
                public void failCallback(JSONObject jsonObject) {
                    presenter.getResponse(jsonObject);
                }
            });

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }*/
}
