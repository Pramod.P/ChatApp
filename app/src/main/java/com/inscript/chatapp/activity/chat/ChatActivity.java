package com.inscript.chatapp.activity.chat;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.inscript.chatapp.R;
import com.inscript.chatapp.adapters.ChatAdapter;
import com.inscript.chatapp.model.Message;
import com.inscript.chatapp.util.SessionManager;
import com.inscript.chatapp.util.Utility;
import com.inscripts.interfaces.SubscribeCallbacks;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import cometchat.inscripts.com.cometchatcore.coresdk.CometChat;

public class ChatActivity extends AppCompatActivity implements ChatContracter.CView {
    @BindView(R.id.chat_recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.chat_input_msg)
    EditText edtMessage;
    @BindView(R.id.chat_send_msg)
    Button btnSend;

    String lastMsgId, userId;
    ChatContracter.CPresenter presenter;
    CometChat cometChat;
    int msgId = 0;
    ArrayList<Message> messageArrayList;
    ChatAdapter adapter = new ChatAdapter(messageArrayList,this,recyclerView);
    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getIntent().getExtras().getString("name"));
        recyclerView.setNestedScrollingEnabled(false);

        cometChat = CometChat.getInstance(this);
        userId = getIntent().getExtras().getString("userId", "");
        lastMsgId = getIntent().getExtras().getString("msgId", "");
        presenter = new ChatPresenter(this);
        messageArrayList = new ArrayList<>();
        Utility.showProgressDialog(this,null);
        presenter.getChatHistory(userId, lastMsgId, cometChat);
        subscribeToChat();

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                msgId = 0;
                if(messageArrayList.size()>0)
                    msgId = Integer.parseInt(messageArrayList.get(messageArrayList.size()-1).getId());

                presenter.sendMsg(userId,++msgId+"",edtMessage.getText().toString(),cometChat);
            }
        });
    }

    public void subscribeToChat()
    {
        cometChat.subscribe(true, new SubscribeCallbacks() {
            @Override
            public void gotOnlineList(JSONObject jsonObject) {

            }

            @Override
            public void gotBotList(JSONObject jsonObject) {
                Log.d("Method","gotBotList");

            }

            @Override
            public void gotRecentChatsList(JSONObject jsonObject) {
                Log.d("Method","gotRecentChatsList"+jsonObject.toString());

            }

            @Override
            public void onError(JSONObject jsonObject) {
                Log.d("Method","onError");

            }

            @Override
            public void onMessageReceived(JSONObject jsonObject) {
                try {
                    Log.d("Method", "onMessageReceived" + jsonObject.toString());
                    //    "message":"Yes bro", "id":26, "from":"50", "sent":1530457109, "self":
                    //    0, "old":0, "message_type":10, "to":46
                    Message message = new Message();
                    message.setId(jsonObject.getString("id"));
                    message.setMessage(jsonObject.getString("message"));
                    message.setFrom(jsonObject.getString("from"));
                    message.setTo(jsonObject.getString("to"));
                    message.setSent(jsonObject.getString("sent"));
                    message.setMessage_type("10");
                    messageArrayList.add(message);

                    int newMsgPosition = messageArrayList.size() - 1;
                    // Notify recycler view insert one new data.
                    adapter.notifyItemInserted(newMsgPosition);
                    // Scroll RecyclerView to the last message.
                    recyclerView.scrollToPosition(newMsgPosition);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void gotProfileInfo(JSONObject jsonObject) {
            }

            @Override
            public void gotAnnouncement(JSONObject jsonObject) {
                Log.d("Method","gotAnnouncement");

            }

            @Override
            public void onAVChatMessageReceived(JSONObject jsonObject) {
                Log.d("Method","onAVChatMessageReceived");

            }

            @Override
            public void onActionMessageReceived(JSONObject jsonObject) {
                Log.d("Method","onActionMessageReceived");

            }

            @Override
            public void onGroupMessageReceived(JSONObject jsonObject) {
                Log.d("Method","onGroupMessageReceived");

            }

            @Override
            public void onGroupsError(JSONObject jsonObject) {
                Log.d("Method","onGroupsError");

            }

            @Override
            public void onLeaveGroup(JSONObject jsonObject) {
                Log.d("Method","onLeaveGroup");

            }

            @Override
            public void gotGroupList(JSONObject groupList) {
                Log.d("Method","gotGroupList\n"+groupList.toString());

            }

            @Override
            public void gotGroupMembers(JSONObject jsonObject) {
                Log.d("Method","gotGroupMembers");
            }

            @Override
            public void onGroupAVChatMessageReceived(JSONObject jsonObject) {
                Log.d("Method","onGroupAVChatMessageReceived");
            }

            @Override
            public void onGroupActionMessageReceived(JSONObject jsonObject) {
                Log.d("Method","onGroupActionMessageReceived");

            }
        });

    }
    @Override
    public void displayChatHistory(JSONObject jsonObject) {
       try {
           Utility.dismissDialog();
           if (jsonObject.get("history") instanceof JSONArray)
           {
                for(int i=0;i<jsonObject.getJSONArray("history").length();i++)
                {
                    Gson gson = new Gson();
                  //  Message message = new Message();
                    Message message = gson.fromJson(jsonObject.getJSONArray("history").getJSONObject(i).toString(),Message.class);
                    messageArrayList.add(message);
                    Log.d("from", messageArrayList.get(i).getFrom());
                }
                Collections.sort(messageArrayList);

                adapter = new ChatAdapter(messageArrayList,this,recyclerView);
                linearLayoutManager = new LinearLayoutManager(this);
                linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        recyclerView.setAdapter(adapter);
                        recyclerView.setLayoutManager(linearLayoutManager);
                    }
                });
                Log.e("Sie",messageArrayList.size()+"");
           }
       }
       catch (Exception e)
       {
           e.printStackTrace();
       }
    }

    @Override
    public void displaySendMessage(JSONObject jsonObject) {
        try {
            Message message = new Message();
            message.setId(jsonObject.getString("id"));
            message.setMessage(jsonObject.getString("message"));
            message.setFrom(new SessionManager(ChatActivity.this).getStringValue(SessionManager.ID));
            message.setTo(jsonObject.getString("from"));
            message.setDirection(jsonObject.getString("direction"));
            message.setSent(jsonObject.getString("sent"));
            message.setMessage_type("10");
            messageArrayList.add(message);

            int newMsgPosition = messageArrayList.size() - 1;

            // Notify recycler view insert one new data.
            adapter.notifyItemInserted(newMsgPosition);

            // Scroll RecyclerView to the last message.
            recyclerView.scrollToPosition(newMsgPosition);

            // Empty the input edit text box.
            edtMessage.setText("");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        imm.hideSoftInputFromWindow(edtMessage.getWindowToken(), 0);
    }
}
