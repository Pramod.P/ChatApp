package com.inscript.chatapp.activity.main;

import android.util.Log;

import org.json.JSONObject;

import cometchat.inscripts.com.cometchatcore.coresdk.CometChat;

public class MainPresenter implements MainContracter.MPresenter
{
    MainContracter.MView mView;
    MainContracter.MModel mModel;

    public MainPresenter(MainContracter.MView mView)
    {
        this.mView = mView;
        this.mModel = new MainModel(this);
    }

    @Override
    public void getResponse(JSONObject jsonObject)
    {
        mView.displayUserProfile();
        Log.d("Data",jsonObject.toString());
    }

    @Override
    public void onProfile(String uid, CometChat context) {
        mModel.getUserProfile(uid,context);
    }

    @Override
    public void onLogout(CometChat cometChat) {
        mModel.logoutUSer(cometChat);
    }

    @Override
    public void getLogoutResponse(JSONObject jsonObject) {
        Log.d("Logout",jsonObject.toString());
        mView.logoutUser();
    }
}
