package com.inscript.chatapp.activity.main;

import android.content.Context;

import org.json.JSONObject;

import cometchat.inscripts.com.cometchatcore.coresdk.CometChat;
import fm.Json;

public class MainContracter
{
    public interface MView
    {
        void displayUserProfile();
        void logoutUser();
    }

    public interface MModel
    {
        void getUserProfile(String uid,CometChat context);
        void logoutUSer(CometChat cometChat);
    }

    public interface MPresenter
    {
        void getResponse(JSONObject jsonObject);
        void getLogoutResponse(JSONObject jsonObject);
        void onProfile(String uid, CometChat context);
        void onLogout(CometChat cometChat);
    }
}
