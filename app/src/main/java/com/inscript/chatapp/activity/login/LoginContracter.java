package com.inscript.chatapp.activity.login;


import android.content.Context;

import org.json.JSONObject;

/**
 * Created by pramodpatil305 on 21-06-2018.
 */

public class LoginContracter
{
    public interface LPresenter
    {
        void sendDatatoModel(String uid,Context context);
        void getResponse(JSONObject jsonObject);
        void onSubmit(String uid,Context context);
    }

    public interface LModel
    {
        void verifyLogin(String uid, Context context);
    }

    public interface LView
    {
        void onSuccess(String message);
        void onFailure(String message);
        boolean validate();
    }
}
