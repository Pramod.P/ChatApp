package com.inscript.chatapp.activity.chat;

import org.json.JSONArray;
import org.json.JSONObject;

import cometchat.inscripts.com.cometchatcore.coresdk.CometChat;

public class ChatContracter
{
    public interface CModel
    {
        void getChatHistory(String userId, String lastMsgId,CometChat cometChat);
        void sendMessage(String userId, String msgId,String msg, CometChat cometChat);
    }

    public interface CView
    {
        void displayChatHistory(JSONObject jsonObject);
        void displaySendMessage(JSONObject jsonObject);
    }

    public interface CPresenter
    {
        void getChatResponse(JSONObject jsonObject);
        void getChatHistory(String userId, String lastMsgId, CometChat cometChat);
        void sendMsg(String userId, String msgId, String msg,CometChat cometChat);
        void sendMsgChatReponse(JSONObject jsonObject);
    }
}
