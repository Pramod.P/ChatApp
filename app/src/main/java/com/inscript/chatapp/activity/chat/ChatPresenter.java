package com.inscript.chatapp.activity.chat;

import org.json.JSONObject;

import cometchat.inscripts.com.cometchatcore.coresdk.CometChat;

public class ChatPresenter implements ChatContracter.CPresenter
{
    ChatContracter.CView cView;
    ChatContracter.CModel cModel;

    public ChatPresenter(ChatContracter.CView cView)
    {
        this.cView = cView;
        cModel = new ChatModel(this);
    }

    @Override
    public void getChatResponse(JSONObject jsonObject)
    {
        cView.displayChatHistory(jsonObject);
    }

    @Override
    public void getChatHistory(String userId, String lastMsgId, CometChat cometChat) {
        cModel.getChatHistory(userId,lastMsgId,cometChat);
    }

    @Override
    public void sendMsg(String userId, String msgId, String msg, CometChat cometChat) {
        cModel.sendMessage(userId,msgId,msg,cometChat);
    }

    @Override
    public void sendMsgChatReponse(JSONObject jsonObject) {
        cView.displaySendMessage(jsonObject);
    }
}
