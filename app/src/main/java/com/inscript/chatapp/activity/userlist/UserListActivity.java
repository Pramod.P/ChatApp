package com.inscript.chatapp.activity.userlist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.inscript.chatapp.R;
import com.inscript.chatapp.model.UserProfile;
import com.inscript.chatapp.adapters.UserAdapter;

import java.util.ArrayList;

public class UserListActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userlist);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ArrayList<UserProfile> userProfiles =  (ArrayList<UserProfile>) getIntent().getSerializableExtra("list");
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerUsers);
        UserAdapter adapter = new UserAdapter(userProfiles,this,recyclerView);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(manager);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
         return true;
    }
}
