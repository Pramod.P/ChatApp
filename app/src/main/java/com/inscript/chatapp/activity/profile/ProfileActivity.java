package com.inscript.chatapp.activity.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.inscript.chatapp.R;
import com.inscript.chatapp.model.UserProfile;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileActivity extends AppCompatActivity
{
    @BindView(R.id.tvUserName)
        TextView tvUserName;
    @BindView(R.id.tvStatus)
        TextView tvStatus;
    @BindView(R.id.tvStatusMsg)
        TextView tvStatusMsg;
    @BindView(R.id.profile_image)
        ImageView imgProfile;

    UserProfile userProfile;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);
        userProfile = (UserProfile) getIntent().getSerializableExtra("user");
        if(userProfile != null) {
            Log.d("user", userProfile.getN() + " " + userProfile.getA() + " " + userProfile.getM());

            Glide.with(this).load("http:" + userProfile.getA())
                    .apply(new RequestOptions()
                            //  .bitmapTransform(new CircleTransform(getActivity()))
                            .diskCacheStrategy(DiskCacheStrategy.ALL))
                    .into(imgProfile);

            tvStatus.setText(userProfile.getS());
            tvStatusMsg.setText(userProfile.getM());
            tvUserName.setText(userProfile.getN());
        }
        else
            onBackPressed();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
}
