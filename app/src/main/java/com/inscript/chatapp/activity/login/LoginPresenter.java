package com.inscript.chatapp.activity.login;

import android.content.Context;

import org.json.JSONObject;


/**
 * Created by pramodpatil305 on 21-06-2018.
 */

public class LoginPresenter implements LoginContracter.LPresenter
{
    LoginContracter.LView lView;
    LoginContracter.LModel lModel;

    @Override
    public void onSubmit(String uid,Context context) {
        if(lView.validate())
            lModel.verifyLogin(uid,context);
    }

    public LoginPresenter(LoginContracter.LView lView)
    {
        this.lView = lView;
        lModel = new LoginModel(this);
    }

    @Override
    public void sendDatatoModel(String uid, Context context)
    {
        lModel.verifyLogin(uid,context);
    }

    @Override
    public void getResponse(JSONObject jsonObject)
    {
        try {
            if (jsonObject.getString("message").equalsIgnoreCase("Login successful"))
                lView.onSuccess(jsonObject.getString("message"));
            else
                lView.onFailure(jsonObject.getString("message"));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

}
