package com.inscript.chatapp.activity.main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.inscript.chatapp.R;
import com.inscript.chatapp.model.UserProfile;
import com.inscript.chatapp.activity.profile.ProfileActivity;
import com.inscript.chatapp.activity.userlist.UserListActivity;
import com.inscript.chatapp.activity.login.LoginActivity;
import com.inscript.chatapp.util.SessionManager;
import com.inscript.chatapp.util.Utility;
import com.inscripts.interfaces.SubscribeCallbacks;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import butterknife.BindView;
import butterknife.ButterKnife;
import cometchat.inscripts.com.cometchatcore.coresdk.CometChat;

public class MainActivity extends AppCompatActivity implements MainContracter.MView
{

    @BindView(R.id.btnProfile)
    Button btnProfile;

    @BindView(R.id.btnLogout)
            Button btnLogout;

    @BindView(R.id.btnUserList)
        Button btnUserList;

    MainContracter.MPresenter presenter;
    CometChat cometChat;
    UserProfile userProfile;
    ArrayList<UserProfile> userProfileArrayList;

    public static final String ACTION ="com.inscript.comechatapp.CUSTOMEMESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        presenter = new MainPresenter(this);
        cometChat = CometChat.getInstance(MainActivity.this);
        userProfileArrayList = new ArrayList<>();

        btnUserList.setBackgroundColor(getResources().getColor(R.color.gray92));
        btnProfile.setBackgroundColor(getResources().getColor(R.color.gray92));
        btnUserList.setEnabled(false);
        btnProfile.setEnabled(false);

        Utility.showProgressDialog(this, "Initialiing");
        btnProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,ProfileActivity.class);
                intent.putExtra("user",userProfile);
                startActivity(intent);
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onLogout(cometChat);
            }
        });

        btnUserList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,UserListActivity.class);
                intent.putExtra("list",userProfileArrayList);
                startActivity(intent);
            }
        });
       cometChat.subscribe(true, new SubscribeCallbacks() {
            @Override
            public void gotOnlineList(JSONObject jsonObject) {
                try {
                    Iterator<?> keys = jsonObject.keys();
                    userProfileArrayList= new ArrayList<>();
                    while (keys.hasNext()) {
                        String key = (String) keys.next();
                        if (jsonObject.get(key) instanceof JSONObject) {
                            Gson gson = new Gson();
                            UserProfile userProfile = gson.fromJson(jsonObject.getJSONObject(key).toString(),UserProfile.class);
                            userProfileArrayList.add(userProfile);
                        }
                    }
                    Log.d("Method", "gotOnlineList\n" + jsonObject.toString());
                }
                catch (Exception ee)
                {
                    ee.printStackTrace();
                }
            }

            @Override
            public void gotBotList(JSONObject jsonObject) {
                Log.d("Method","gotBotList");

            }

            @Override
            public void gotRecentChatsList(JSONObject jsonObject) {
                Log.d("Method","gotRecentChatsList");

            }

            @Override
            public void onError(JSONObject jsonObject) {
                Log.d("Method","onError");

            }

            @Override
            public void onMessageReceived(JSONObject jsonObject) {
                Log.d("Method","onMessageReceived");

            }

            @Override
            public void gotProfileInfo(JSONObject jsonObject) {
                Log.d("Method","gotProfileInfo\n"+jsonObject.toString());
                Gson gson = new Gson();
                userProfile = gson.fromJson(jsonObject.toString(), UserProfile.class);
                Log.d("User Profile",userProfile.toString());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        btnUserList.setBackgroundColor(getResources().getColor(R.color.blue_dark));
                        btnProfile.setBackgroundColor(getResources().getColor(R.color.blue_dark));
                        btnUserList.setEnabled(true);
                        btnProfile.setEnabled(true);
                        new SessionManager(MainActivity.this).setStringValue(SessionManager.ID,userProfile.getId());
                    }
                });
            }

            @Override
            public void gotAnnouncement(JSONObject jsonObject) {
                Log.d("Method","gotAnnouncement");
            }

            @Override
            public void onAVChatMessageReceived(JSONObject jsonObject) {
                Log.d("Method","onAVChatMessageReceived");

            }

            @Override
            public void onActionMessageReceived(JSONObject jsonObject) {
                Log.d("Method","onActionMessageReceived");

            }

            @Override
            public void onGroupMessageReceived(JSONObject jsonObject) {
                Log.d("Method","onGroupMessageReceived");

            }

            @Override
            public void onGroupsError(JSONObject jsonObject) {
                Log.d("Method","onGroupsError");

            }

            @Override
            public void onLeaveGroup(JSONObject jsonObject) {
                Log.d("Method","onLeaveGroup");

            }

            @Override
            public void gotGroupList(JSONObject groupList) {
                Log.d("Method","gotGroupList\n"+groupList.toString());

            }

            @Override
            public void gotGroupMembers(JSONObject jsonObject) {
                Log.d("Method","gotGroupMembers");

            }

            @Override
            public void onGroupAVChatMessageReceived(JSONObject jsonObject) {
                Log.d("Method","onGroupAVChatMessageReceived");

            }

            @Override
            public void onGroupActionMessageReceived(JSONObject jsonObject) {
                Log.d("Method","onGroupActionMessageReceived");

            }
        });
    }

    @Override
    public void displayUserProfile() {

    }

    @Override
    public void logoutUser() {
        Toast.makeText(this,"Logout Successfully",Toast.LENGTH_SHORT).show();
        new SessionManager(this).clearData();
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver,new IntentFilter(ACTION));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("CometChat","chat initialized");
            Utility.dismissDialog();
        }
    };
}
