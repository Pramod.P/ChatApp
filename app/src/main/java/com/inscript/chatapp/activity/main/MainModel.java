package com.inscript.chatapp.activity.main;

import android.util.Log;

import com.inscripts.interfaces.Callbacks;

import org.json.JSONObject;

import cometchat.inscripts.com.cometchatcore.coresdk.CometChat;

public class MainModel implements MainContracter.MModel
{
    MainContracter.MPresenter presenter;

    public MainModel(MainContracter.MPresenter presenter)
    {
        this.presenter = presenter;
    }


    @Override
    public void getUserProfile(String uid, CometChat cometChat)
    {
        try {
          //  CometChat cometChat = CometChat.getInstance(context);

            cometChat.getUserInfo(uid, new Callbacks()
            {
                @Override
                public void successCallback(JSONObject jsonObject) {
                    Log.d("Success",jsonObject.toString());
                    presenter.getResponse(jsonObject);

                }
                @Override
                public void failCallback(JSONObject jsonObject) {
                    Log.d("Reponse",jsonObject.toString());
                    presenter.getResponse(jsonObject);
                }
            });

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void logoutUSer(CometChat cometChat) {
        cometChat.logout(new Callbacks() {
            @Override
            public void successCallback(JSONObject jsonObject) {
                Log.d("Logout",jsonObject.toString());
                presenter.getLogoutResponse(jsonObject);
            }

            @Override
            public void failCallback(JSONObject jsonObject) {
                Log.d("Logout Error",jsonObject.toString());
            }
        });
    }
}
