package com.inscript.chatapp.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.inscript.chatapp.R;
import com.inscript.chatapp.activity.main.MainActivity;
import com.inscript.chatapp.util.SessionManager;
import com.inscript.chatapp.util.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements LoginContracter.LView
{
    @BindView(R.id.edtUID)
    EditText edtUID;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    LoginPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frag_login);
        ButterKnife.bind(this);

        presenter = new LoginPresenter(this);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.showProgressDialog(LoginActivity.this,null);
                presenter.onSubmit(edtUID.getText().toString(),LoginActivity.this);
            }
        });
    }

    @Override
    public void onFailure(String message)
    {
        Utility.dismissDialog();
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(String message)
    {
        Utility.dismissDialog();
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
        new SessionManager(this).setStringValue(SessionManager.USER_ID,edtUID.getText().toString());
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }


    @Override
    public boolean validate() {
        if(TextUtils.isEmpty(edtUID.getText().toString()))
        {
            Toast.makeText(this, "Please enter the User ID", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
