package com.inscript.chatapp.activity.splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.inscript.chatapp.R;
import com.inscript.chatapp.activity.main.MainActivity;
import com.inscript.chatapp.activity.login.LoginActivity;
import com.inscript.chatapp.util.SessionManager;

public class SplashActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
       try {
           Thread.sleep(2000);
            if(new SessionManager(this).getStringValue(SessionManager.USER_ID).equals(""))
                startActivity(new Intent(this, LoginActivity.class));
            else
                startActivity(new Intent(this, MainActivity.class));
            finish();
       }
       catch (Exception e)
       {
           e.printStackTrace();
       }

    }
}
