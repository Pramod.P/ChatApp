package com.inscript.chatapp.activity.chat;

import android.util.Log;

import com.inscripts.interfaces.Callbacks;

import org.json.JSONObject;

import cometchat.inscripts.com.cometchatcore.coresdk.CometChat;

public class ChatModel implements ChatContracter.CModel
{
    ChatContracter.CPresenter presenter;

    public ChatModel(ChatContracter.CPresenter presenter)
    {
        this.presenter = presenter;
    }

    @Override
    public void getChatHistory(String userId, String lastMsgId, CometChat cometChat) {
        try {
            cometChat.getChatHistory(Long.parseLong(userId), Long.parseLong(lastMsgId), new Callbacks() {
                @Override
                public void successCallback(JSONObject response)
                {
                    Log.d("Respose",response.toString());
                    presenter.getChatResponse(response);
                }

                @Override
                public void failCallback(JSONObject response)
                {
                    Log.d("Respose",response.toString());
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void sendMessage(String userId, String msgId, String msg, CometChat cometChat) {
        cometChat.sendMessage(Long.parseLong(msgId),userId,msg, false, new Callbacks(){
            @Override
            public void successCallback(JSONObject response){
                Log.d("Message",response.toString());
                presenter.sendMsgChatReponse(response);
            }

            @Override
            public void failCallback(JSONObject response)
            {
                Log.d("Message",response.toString());
            }
        });
    }
}
