package com.inscript.chatapp;

import android.app.Application;
import android.content.Intent;
import android.util.Log;

import com.inscript.chatapp.activity.main.MainActivity;
import com.inscript.chatapp.model.UserProfile;
import com.inscripts.interfaces.Callbacks;

import org.json.JSONObject;

import cometchat.inscripts.com.cometchatcore.coresdk.CometChat;

public class AppController extends Application
{
    static UserProfile userProfile;

    @Override
    public void onCreate() {
        super.onCreate();

        CometChat cometChat = CometChat.getInstance(this);
        cometChat.initializeCometChat("", getResources().getString(R.string.license_key),getResources().getString(R.string.api_key),
                true, new Callbacks() {
            @Override
            public void successCallback(JSONObject jsonObject) {
                Log.d("Reponse",jsonObject.toString());
                Intent i = new Intent(MainActivity.ACTION);
                i.putExtra("SOME", "success");
                sendBroadcast(i);
            }
            @Override
            public void failCallback(JSONObject jsonObject) {
                Log.d("Fail",jsonObject.toString());
            }
        });
    }
}
